// This file would contain the client side layer of code
// Buttons would lie here, whereas the functions that the buttons activate would be 
// on the server layer
// The code here sends a post request on the url that is being "listened to" by the server code

console.log('Client Side Code');

const button = document.getElementById('myButton');
button.addEventListener('click', function(event) {
    console.log('button was clicked');
// setting the method as a POST request on the '/clicked' page? => not sure the terminology here
    fetch('/clicked', {method: 'POST'}) 
        .then(function(response) {
            if(response.ok) {
                console.log('Click Successful');
                return;
            }
            throw new Error('Request Failed');
        })
        .catch(function(error) {
            console.log(error);
        });
});

