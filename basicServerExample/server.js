// This is a more robust "server" that handles requests and can run back end functions
// when sent information from the front end

// Intializing Express (A Web Application Framework for Node, makes parsing HTTP easier)
const express = require("express");
const app = express();

// Initializing HTTP
const http = require("http");
const bodyParser = require("body-parser");

// Creating the "main" server
const server = http.createServer(app);

app.use(bodyParser.urlencoded());
app.use(bodyParser.json())

// Adding a request listener to handle incoming requests and return a response
// This listener will return a log and a success code
// The '/' means that the listener is waiting for the front end to send a POST request on the
// route '/', which is the home page of your URL, http://localhost:8000/
// If you set the POST function to be on '/clicked', then you have to listen for '/clicked'
// You can also call a function doing this, allowing you to update the front end

// Data input ex.
// This would be what the post method forms based on what you pass in 
// ------------- Input ----------------
// <form method="post" action="/">
//   <input type="test" name="field1">
//   <input type="test" name="field2">
//   <input type="submit">
// </form>

// ------------- Output ----------------
// { field1: 'form contents', field2: 'second field contents' }
// Did something

const testFunction = function() {
    console.log('\nDid something');
};

app.post('/clicked', function(request, response) {
    console.log(request.body);
    response.send(200);

    // Sending a response doesn't stop the function, you can send back a 200 then keep going
    testFunction();
});

// Initializing what IP:PORT the server is listening on, ex.) localhost:8000
server.listen(process.env.PORT, process.env.IP);





