const http = require("http");

const host = 'localhost';
const port = 8000;

const requestListener = function (request, response) {
    response.setHeader("Content-Type", "text/csv");
    response.setHeader("Content-Disposition", "attachment;filename=oceanpals.csv");
    response.writeHead(200);
    response.end(`id,name,email\n1,Sammy Shark,shark@ocean.com`);
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});
