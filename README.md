# Server Communication Example

Just a thrown together set of files to show how basic webpage/server communication is set up, in this case using node.js which is what I have experience in. 

Any other format or framework will need different syntax, but I'm using this to get the basics of how communication works here.

The basic js files that are paired with pictures are simple node servers that just handle one type of request, they're also just using http and node, there isn't a framework being used to help handle requests sent. They're just examples of how data is sent/shown using http requests. 

The server example uses node and express javascript libraries to handle Http requests and to run a node server alongside an html file that contains a button to press. 